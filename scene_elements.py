from typing import Tuple
from pygame import Rect, Surface
from pygame.font import Font
from typing import List
from re import search, IGNORECASE

WHITE = (255,255,255)
RED = (255, 0, 0)

"""
Base class
"""
class SceneElement():

    def __init__(self) -> None:
        self.updated = True # This variable can be used to tell the scene element if anything has changed since last get_blit.
        self.surface = None
        self.rect = None
    
    def process_changes(self) -> None:
        raise NotImplementedError(f"The process_changes function in {self.__name__} must be defined.")

    """
    Returns the data to be blitted.
    """
    def get_blit(self) -> Tuple[Surface, Rect]:
        if self.updated:
            self.updated = False
            self.process_changes()
        return (self.surface, self.rect)

class TextBox(SceneElement):

    def __init__(self, text: str, location: tuple, font: Font, pivot="topleft", colours={}, alphas={}, default_colour=WHITE):
        super().__init__()
        self.text = text
        self.location = location
        self.pivot = pivot
        self.colours = colours.copy()
        self.default_colour = default_colour
        self.font = font
        self.counter = 0
        self.process_changes()

    def set_colour(self, colour):
        self.colours.clear()
        self.colours[len(self.text)] = (colour, 255)
        self.default_colour = colour
        self.updated = True
        

    def set_text(self, text):
        self.text = text
        self.updated = True

    def get_text(self):
        return self.text

    def move(self, new_location):
        self.location = new_location
        self.updated = True

    def process_changes(self):
        portions = []
        running_width = 0

        last_key = 0
        running_width = 0

        colour_lengths = list(self.colours.keys())
        if not self.colours or (colour_lengths and max(colour_lengths) < len(self.text)):
            self.colours[len(self.text)] = (self.default_colour, 255)

        # The above dict needs to be in order lowest to highest or the below will not work.        
        for key in sorted(self.colours):
            txt_object = self.font.render(self.text[ last_key : key ], True, self.colours[key][0])
            txt_object.set_alpha(self.colours[key][1])
            txt_rect = txt_object.get_rect()
            txt_rect.left = running_width
            running_width += txt_rect.width
            portions.append((txt_object, txt_rect))
            last_key = key
        
        text_surface = Surface((running_width, txt_rect.height))
        for portion in portions:
            text_surface.blit(portion[0], portion[1])
        
        self.rect = text_surface.get_rect()
        setattr(self.rect, self.pivot, (self.location[0], self.location[1]))
        self.surface = text_surface
        self.updated = True


class MonitorBox(TextBox):
    def __init__(self, label: str, location: tuple, font: Font, initial_value="0", pivot="topleft", colours={}, alphas={}, default_colour=WHITE):
        self.label = label
        self.value = initial_value
        text = label + initial_value
        super().__init__(text, location, font, pivot=pivot, colours=colours, alphas=alphas, default_colour=default_colour)

    def update_value(self, value):
        self.value = str(value)
        self.set_text(self.label + self.value)

class InputBox(TextBox):
    def __init__(self, location: tuple, font: Font, label="", initial_value="", pivot="topleft", colours={}, alphas={}, default_colour=WHITE):
        self.input_value = initial_value
        self.label = label
        text = self.label + self.input_value
        super().__init__(text, location, font, pivot=pivot, colours=colours, alphas=alphas, default_colour=default_colour)

    def clear(self):
        self.input_value = ""
        self.set_text(self.label + self.input_value)

    def backspace(self):
        self.input_value = self.input_value[:-1]
        self.set_text(self.label + self.input_value)
    
    def add_character(self, input):
        self.input_value += str(input)
        self.set_text(self.label + self.input_value)

class ScrollableSelectionBox(SceneElement):
    def __init__(self, elements: List[str], location: List[int], font: Font, maximum_visible=3, selection_colour=RED, default_colour=WHITE, pivot="topleft"):
        super().__init__()
        self.elements = []
        self.text_elements = elements
        self.filtered_out = []
        self.default_colour = default_colour
        self.font = font
        self.pivot = pivot
        self.location = location
        self.selection = 0
        self.selection_colour = selection_colour
        self.maximum_visible = maximum_visible
        self.visible_range = [0, self.maximum_visible]
        self.process_changes()

    """
    Method for filtering out items that do not match the term.
    """
    def set_filter(self, term):
        self.filtered_out.clear()
        if term:
            search_re = f"(\s|^){term}"
            self.filtered_out = [term for term in self.text_elements if not search(search_re, term, IGNORECASE)]

        self.selection = 0
        self.updated = True
    
    def clear_filters(self):
        self.filtered_out.clear()
        self.updated = True

    def add_item(self, text):
        self.text_elements.append(text)
        self.updated = True

    def contains_elements(self) -> bool:
        return bool(self.text_elements)

    """
    Returns the selected item and removes it.
    """
    def remove_selected_item(self) -> str:
        text = self.get_selected_text()
        self.text_elements.pop(self.visible_range[0] + self.selection)
        self.updated = True
        return text

    def scroll(self, direction: int):
        scrollable_view = min(len(self.elements), self.maximum_visible)
        if len(self.text_elements) > scrollable_view:
            if self.selection + direction < 0 or self.selection + direction > scrollable_view - 1:
                self.visible_range[0] += direction
                self.visible_range[1] += direction
            else:
                self.selection += direction

        else:
            self.selection += direction
            if self.selection < 0:
                self.selection = scrollable_view - 1
            if self.selection > scrollable_view - 1:
                self.selection = 0
            


        self.updated = True

    def get_selected_text(self):
        if len(self.elements) > 0:
            return self.elements[self.selection].get_text()
        return ""

    def move_selection_to_end(self):
        self.visible_range = [max(len(self.text_elements) - self.maximum_visible, 0), len(self.text_elements)]
        self.selection = min(len(self.text_elements) - 1, self.maximum_visible - 1)
        self.updated = True

    def reset(self):
        self.selection = 0
        self.updated = True
        
    def clear_elements(self) -> None:
        self.text_elements.clear()
        self.updated = True
        

    def process_changes(self):

        self.elements.clear()

        elems = [x for x in self.text_elements if x not in self.filtered_out]
        
        if not elems:
            if not self.surface:
                self.surface = Surface((0,0))
                self.rect = self.surface.get_rect()
            self.surface.fill((0,0,0))
            return

        processed_indexes = []

        for index in range(*self.visible_range):
            index = index % len(elems)
            if index not in processed_indexes:
                processed_indexes.append(index)
                e = TextBox(elems[index], (0,0), self.font, default_colour=self.default_colour)
                self.elements.append(e)

        # Get the width and height of longest element
        le = self.font.render(max(elems, key=len), True, (0,0,0)).get_rect()
        width = le.width
        height = le.height * len(self.elements)

        self.surface = Surface((width, height))
        self.rect = self.surface.get_rect()
        setattr(self.rect, self.pivot, self.location)

        self.selection = self.selection if self.selection < len(self.elements) - 1 else len(self.elements) - 1

        for index, element in enumerate(self.elements):
            x, y = 0, 0 + (index * element.rect.height)
            element.move((x, y))
            if index == self.selection:
                element.set_colour(self.selection_colour)

            self.surface.blit(*element.get_blit())
    


            


class Divider(SceneElement):
    pass

