import fitbit
import cherrypy
import sys
import threading
import traceback
import webbrowser

from urllib.parse import urlparse
from fitbit.api import Fitbit
from oauthlib.oauth2.rfc6749.errors import MismatchingStateError, MissingTokenError
from json import loads, dumps
import datetime


class OAuth2Server:
    def __init__(self, client_id, client_secret,
                 redirect_uri='http://127.0.0.1:8080/'):
        """ Initialize the FitbitOauth2Client """
        self.success_html = """
            <h1>You are now authorized to access the Fitbit API!</h1>
            <br/><h3>You can close this window</h3>"""
        self.failure_html = """
            <h1>ERROR: %s</h1><br/><h3>You can close this window</h3>%s"""

        self.fitbit = Fitbit(
            client_id,
            client_secret,
            redirect_uri=redirect_uri,
            timeout=10,
        )

        self.redirect_uri = redirect_uri

    def browser_authorize(self):
        """
        Open a browser to the authorization url and spool up a CherryPy
        server to accept the response
        """
        url, _ = self.fitbit.client.authorize_token_url()
        # Open the web browser in a new thread for command-line browser support
        threading.Timer(1, webbrowser.open, args=(url,)).start()

        # Same with redirect_uri hostname and port.
        urlparams = urlparse(self.redirect_uri)
        cherrypy.config.update({'server.socket_host': urlparams.hostname,
                                'server.socket_port': urlparams.port})

        cherrypy.quickstart(self)

    @cherrypy.expose
    def index(self, state, code=None, error=None):
        """
        Receive a Fitbit response containing a verification code. Use the code
        to fetch the access_token.
        """
        error = None
        if code:
            try:
                self.fitbit.client.fetch_access_token(code)
            except MissingTokenError:
                error = self._fmt_failure(
                    'Missing access token parameter.</br>Please check that '
                    'you are using the correct client_secret')
            except MismatchingStateError:
                error = self._fmt_failure('CSRF Warning! Mismatching state')
        else:
            error = self._fmt_failure('Unknown error while authenticating')
        # Use a thread to shutdown cherrypy so we can return HTML first
        self._shutdown_cherrypy()
        return error if error else self.success_html

    def _fmt_failure(self, message):
        tb = traceback.format_tb(sys.exc_info()[2])
        tb_html = '<pre>%s</pre>' % ('\n'.join(tb)) if tb else ''
        return self.failure_html % (message, tb_html)

    def _shutdown_cherrypy(self):
        """ Shutdown cherrypy in one second, if it's running """
        if cherrypy.engine.state == cherrypy.engine.states.STARTED:
            threading.Timer(1, cherrypy.engine.exit).start()



# My stuff.

class CustomFitbit(Fitbit):
    def log_food(self, data):
        """
        https://dev.fitbit.com/docs/sleep/#log-sleep
        Not sure why the hell I need to add this, but whatev.
        """
        
        url = "{0}/{1}/user/-/foods/log.json".format(*self._get_common_args())
        return self.make_request(url, data=data, method="POST")

def get_gram_unit_id(fitbit_api):
    units = fitbit_api.food_units()

    for element in units:
        if element["name"] == "gram":
            return element["id"]

def get_now_mealtime_id():
    dt = datetime.datetime.now()
    
    if dt.hour < 12:
        return 1
    if dt.hour < 18:
        return 3
    if dt.hour < 24:
        return 5

    
def get_active_fitbit():
    try:
        creds = loads(open("fitbit_credentials.json", "r").read())
    except FileNotFoundError:
        print("No credentials exist. Please create fitbit_credentials.json and populate at least `client_id` and `client_secret`.")
        return

    while True:
        try:
            unauth_client = CustomFitbit('', '', access_token=creds.get('access_token', None), refresh_token=creds.get('refresh_token', None))
            unauth_client.food_units()
            break
        except fitbit.exceptions.HTTPUnauthorized:
            server = OAuth2Server(creds["client_id"], creds['client_secret'])
            server.browser_authorize()
            for key, value in server.fitbit.client.session.token.items():
                creds[key] = value
        
    open("fitbit_credentials.json", "w").write(dumps(creds))
    return unauth_client