

def patch():
    import sys
    import importlib
    module = importlib.import_module('mock')
    sys.modules['RPi.GPIO'] = module
    sys.modules['RPi'] = module
    sys.modules['hx711py.hx711'] = module

class HX711:
    def __init__(self, a: int, b: int) -> None:
        pass

    def set_reading_format(self, a: str, b: str):
        pass

    def reset(self):
        pass

    def tare(self):
        pass

    def set_reference_unit(self, a: float):
        pass

    def get_weight(self) -> float:
        return 1000

class GPIO:
    BCM = 123
    IN = 456
    OUT = 789

    def setmode(a: int):
        pass
    def setup(a: int, b: int):
        pass
    def input(a: int):
        return True
    def output(a: int, b: int):
        pass
    def cleanup():
        pass