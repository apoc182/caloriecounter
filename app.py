import pygame
from json import loads
from os import environ


try:
    pygame.init()

    settings = loads(open("settings.json", "r").read())

    # Use mock objects if settings decree.
    if settings["debug"]:
        from mock import patch
        patch()

    from hx711py.hx711 import HX711
    import RPi.GPIO as GPIO
    from scenes import Main, SceneEnum, ModeSelectionScene

    # Initialise some pygame stuff
    infoObject = pygame.display.Info()
    if settings["fullscreen"]:
        environ['SDL_VIDEO_CENTERED'] = '1'
        settings["resolution"] = (infoObject.current_w, infoObject.current_h)
        screen = pygame.display.set_mode(settings["resolution"], pygame.NOFRAME)
    else:
        screen = pygame.display.set_mode(settings["resolution"])
    pygame.mouse.set_visible(False)

    #screen = pygame.display.set_mode(settings["resolution"])
    pygame.display.set_caption('Calorie Counter')
    clock = pygame.time.Clock()
    delta = 0
    print("initialising hx711")
    hx711 = HX711(settings["d_out_pin"], settings["pd_sck_pim"])
    hx711.set_reading_format("MSB", "MSB")
    hx711.reset()
    hx711.tare()
    hx711.set_reference_unit(settings["reference_unit"] / settings["reference_weight"])
    print("hx711 initialise")


    mode_selection_scene = ModeSelectionScene(settings["resolution"])
    one_at_a_time_scene = Main(settings["resolution"], hx711, settings["weight_precision"], settings["json_filename"], False)
    all_at_once_scene = Main(settings["resolution"], hx711, settings["weight_precision"], settings["json_filename"], True)

    scene_map = {
        SceneEnum.ONEATATIME : one_at_a_time_scene,
        SceneEnum.MODE_SELECTION : mode_selection_scene,
        SceneEnum.ALLATONCE : all_at_once_scene
    }


    

    current_scene = mode_selection_scene
    run = True

    while run:
        # Fill the screen with nothing.
        screen.fill(settings["background_colour"])
        events = pygame.event.get()
        transition = current_scene.update(screen, events, delta)
        if transition != -1:
            current_scene = scene_map[transition]

        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    transition = current_scene.escape()
                    if transition == SceneEnum.QUIT:
                        run = False
                        break
                    elif transition != -1:
                        current_scene = scene_map[transition]

        pygame.display.update()
finally:
    GPIO.cleanup()
