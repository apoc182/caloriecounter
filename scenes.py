from pygame import Surface, KEYDOWN
from pygame.font import Font
from scene_elements import InputBox, TextBox, ScrollableSelectionBox, MonitorBox
from typing import List
from pygame.event import Event
from pygame import K_UP, K_DOWN, K_RETURN, K_BACKSPACE, K_LEFT, K_RIGHT, K_DELETE, K_n, K_t, K_s, KMOD_CTRL
from hx711py.hx711 import HX711
from requests import get
from threading import Thread
from json import loads, dumps
from pygame.key import get_mods
from food_logger import get_active_fitbit, get_gram_unit_id, get_now_mealtime_id
import datetime

class SceneEnum():
    MODE_SELECTION = 0
    ONEATATIME = 1
    ALLATONCE = 2
    QUIT = 3

class AddedItem():
    def __init__(self, name, weight, calories):
        self.name = name
        self.weight = weight
        self.calories = calories
    
    def __str__(self):
        return f"{int(self.weight)}g of {self.name} ({int(self.calories)}cal)" 

"""
Abstract type class that all scenes derrive from.
"""
class Scene():
    def __init__(self, resolution: List[int]):
        self.this_font = Font('freesansbold.ttf', 32)
        self.resolution = resolution

    def update(self, screen: Surface, events: List[Event], delta: float) -> None:
        raise NotImplementedError("Each scene must have update implemented.")

    def escape(self) -> None:
        raise NotImplementedError("Each scene must have escape implemented.")

class ModeSelectionScene(Scene):

    def __init__(self, resolution: List[int]):
        super().__init__(resolution)
        self.selection = ScrollableSelectionBox(["All at once", "One at a time"], (resolution[0]//2, resolution[1]//2), self.this_font, maximum_visible=3, pivot="center")

    
    def update(self, screen, events, delta):
        screen.blit(*self.selection.get_blit())

        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_UP:
                    self.selection.scroll(-1)
                if event.key == K_DOWN:
                    self.selection.scroll(1)
                if event.key == K_RETURN:
                    selection = self.selection.get_selected_text()
                    if selection == "All at once":
                        return SceneEnum.ALLATONCE
                    else:
                        return SceneEnum.ONEATATIME
        return -1
        
    def escape(self) -> None:
        return SceneEnum.QUIT

class ScalesScene(Scene):
    def __init__(self, resolution: List[int], hx711: HX711, precision: int, json_filename: str):
        super().__init__(resolution)
        self.food_map = {}
        self.food_name_list = []
        self.precision = precision
        self.hx711 = hx711
        self.json_filename = json_filename
        self.added_items_display = ScrollableSelectionBox([], (0, resolution[1]), self.this_font, maximum_visible=5, pivot="bottomleft")
        self.added_items = []
        self.current_tare = 0
        self.running_calories = 0
        self.running_weight = 0
        self.current_calories_display = MonitorBox("Scale Calories: ", (resolution[0], 0), self.this_font, pivot="topright")
        self.current_fibre_display = MonitorBox("Scale Fibre: ", (resolution[0], self.current_calories_display.rect.height), self.this_font, pivot="topright")
        self.display_calories = MonitorBox("Total Calories: ", (resolution[0], self.current_calories_display.rect.height * 2), self.this_font, pivot="topright")
        self.weight_display = MonitorBox("Weight: ", (resolution[0], resolution[1]), self.this_font, pivot="bottomright")
        self.weight_thread = None
        self.live_weight = 0
        self.live_calories = 0
        self.live_fibre = 0
        self.load_data()

    def drop_zeros(self, value):
        if int(value) == value:
            return int(value)
        return value
    
    def get_weight_async(self):
        self.live_weight = round(self.hx711.get_weight(), 0)

    def update(self, screen, events, delta):

        self.display_calories.update_value(self.drop_zeros(self.running_calories))
        self.current_calories_display.update_value(self.drop_zeros(round((self.live_calories / 100) * (self.live_weight - self.current_tare), 0)))
        self.current_fibre_display.update_value(self.drop_zeros(round((self.live_fibre / 100) * (self.live_weight - self.current_tare), 0)))
        if not self.weight_thread or not self.weight_thread.is_alive():
            self.weight_display.update_value(self.drop_zeros(self.live_weight - self.current_tare))
            self.weight_thread = Thread(target=self.get_weight_async)
            self.weight_thread.start()
    
    def load_data(self):
        self.food_map = loads(open(self.json_filename, "r").read())
        self.food_list = [ x + " (" + str(self.food_map[x]["calories"]) + "cal/100g)" for x in list(self.food_map.keys())]
        self.food_list.sort()
    
    def update_data(self):
        open(self.json_filename, "w").write(dumps(self.food_map))
        self.load_data()
    
    def escape(self) -> None:
        self.current_tare = 0
        self.running_calories = 0
        self.running_weight = 0
        self.live_weight = 0
        self.display_calories.update_value("0")
        self.weight_display.update_value("0")
        self.weight_thread = None
        self.added_items_display.clear_elements()

    def _update_values(self, name: str):
        current_entry = self.food_map[name[:name.rfind(" (")]]
        self.live_calories = current_entry.get("calories", 0)
        self.live_fibre = current_entry.get("fibre", 0)


class Main(ScalesScene):

    class MainStates():
        INIT = 0
        RETARE = 1
        SELECT_ITEM = 2
        ADD_ITEM = 3
        SAVE_MEAL = 4

    def __init__(self, resolution: List[int], hx711: HX711, precision: int, csv_link: str, running_tare: bool):
        super().__init__(resolution, hx711, precision, csv_link)
        self.food_display = ScrollableSelectionBox(self.food_list, (0, self.resolution[1]//2), self.this_font, pivot="midleft")
        self.input_box = InputBox((0, 0), self.this_font)
        if running_tare:
            self.tare_label = TextBox("Please place tare object", (resolution[0]//2, resolution[1]//2), self.this_font, pivot="center")
            self.tare_label2 = TextBox("on the scales and press enter.", (resolution[0]//2, resolution[1]//2 + self.tare_label.rect.height), self.this_font, pivot="center")
            self.current_state = Main.MainStates.INIT
        else:
            self.current_state = Main.MainStates.SELECT_ITEM
        self.running_tare = running_tare

        self.add_item_message = TextBox("X", (resolution[0]//2, resolution[1]//2), self.this_font, pivot="center")
        self.add_item_message2 = TextBox("Please enter the energy content of per 100 grams", (resolution[0]//2, resolution[1]//2 + self.add_item_message.rect.height), self.this_font, pivot="center")
        self.add_item_input = InputBox((resolution[0]//2, resolution[1]//2 + self.add_item_message.rect.height * 2), self.this_font, pivot="midtop")
        self.add_item_unit_select = ScrollableSelectionBox(["calories", "kilojoules"], (resolution[0]//2, resolution[1]//2 + self.add_item_message.rect.height * 3), self.this_font, pivot="midtop")
        self.item_to_add_name = ""
        self.item_to_add_cal_per_100_g = 0

        # Create save to fitbit menu.
        self.save_to_pantry_or_fitbit_message1 = TextBox("Please enter a name for this meal:", (resolution[0]//2, resolution[1]//2), self.this_font, pivot="center")
        self.save_to_pantry_or_fitbit_input = InputBox((resolution[0]//2, resolution[1]//2 + self.save_to_pantry_or_fitbit_message1.rect.height//2), self.this_font, pivot="midtop", colours={999:[(255,0,0), 255]})
        self.save_to_pantry_or_fitbit_message2 = TextBox("and then...", (resolution[0]//2, resolution[1]//2 + self.save_to_pantry_or_fitbit_message1.rect.height * 2), self.this_font, pivot="center")
        self.save_to_pantry_or_fitbit_selection = ScrollableSelectionBox(["Eat now?", "Add to pantry?"], (resolution[0]//2, resolution[1]//2 + self.save_to_pantry_or_fitbit_message1.rect.height * 3), self.this_font, pivot="midtop")



        
    def escape(self) -> None:
        self.add_item_unit_select.reset()
        self.item_to_add_name = ""
        self.item_to_add_cal_per_100_g = 0
        self.add_item_input.clear()

        if self.current_state == Main.MainStates.SELECT_ITEM or self.current_state == Main.MainStates.INIT:
            super().escape()
            self.food_display.reset()
            self.input_box.clear()
            self.current_state = Main.MainStates.INIT
            return SceneEnum.MODE_SELECTION
        else:
            self.current_state = Main.MainStates.SELECT_ITEM
            return -1
        

    def add_new_item(self, name):
        weight  = round(self.hx711.get_weight() - self.current_tare, self.precision)
        calories = round(self.food_map[name]["calories"] * (weight/100), self.precision)
        item = AddedItem(name, weight, calories)
        self.running_calories += calories
        self.running_weight += weight
        self.added_items_display.add_item(str(item))
        self.added_items.append(item)
        self.added_items_display.move_selection_to_end()
        if self.running_tare:
            self.current_state = Main.MainStates.RETARE
        

        
    def update(self, screen, events, delta):
        super().update(screen, events, delta)

        if self.current_state == Main.MainStates.INIT:
            screen.blit(*self.tare_label.get_blit())
            screen.blit(*self.tare_label2.get_blit())
            name = self.food_display.get_selected_text()
            if name:
                self._update_values(name)
        elif self.current_state == Main.MainStates.RETARE:
            self.current_tare = round(self.hx711.get_weight(), self.precision)
            self.current_state = Main.MainStates.SELECT_ITEM
        elif self.current_state == Main.MainStates.ADD_ITEM:
            screen.blit(*self.add_item_message.get_blit())
            screen.blit(*self.add_item_message2.get_blit())
            screen.blit(*self.add_item_unit_select.get_blit())
            screen.blit(*self.add_item_input.get_blit())
        elif self.current_state == Main.MainStates.SELECT_ITEM:
            screen.blit(*self.input_box.get_blit())
            screen.blit(*self.food_display.get_blit())
            screen.blit(*self.added_items_display.get_blit())
            screen.blit(*self.weight_display.get_blit())
            screen.blit(*self.current_calories_display.get_blit())
            screen.blit(*self.current_fibre_display.get_blit())
            screen.blit(*self.display_calories.get_blit())
        elif self.current_state == Main.MainStates.SAVE_MEAL:
            screen.blit(*self.save_to_pantry_or_fitbit_message1.get_blit())
            screen.blit(*self.save_to_pantry_or_fitbit_input.get_blit())
            screen.blit(*self.save_to_pantry_or_fitbit_message2.get_blit())
            screen.blit(*self.save_to_pantry_or_fitbit_selection.get_blit())

        for event in events:
            if event.type == KEYDOWN:
                if self.current_state == Main.MainStates.ADD_ITEM:
                    if event.key == K_UP:
                        self.add_item_unit_select.scroll(-1)
                    elif event.key == K_DOWN:
                        self.add_item_unit_select.scroll(1)
                    elif event.unicode.isnumeric():
                        self.add_item_input.add_character(event.unicode)
                    elif event.key == K_BACKSPACE:
                        self.add_item_input.backspace()
                    elif event.key == K_RETURN and self.add_item_input.get_text():
                        unit = int(self.add_item_input.get_text())
                        if self.add_item_unit_select.get_selected_text() == "kilojoules":
                            unit /= 4.184
                        self.item_to_add_cal_per_100_g = self.drop_zeros(round(unit, self.precision))
                        self.food_map[self.item_to_add_name]["calories"] = self.item_to_add_cal_per_100_g
                        self.update_data()
                        self.food_display.text_elements = self.food_list
                        self.food_display.clear_filters()
                        self.add_new_item(self.item_to_add_name)
                        self.input_box.clear()
                        self.add_item_input.clear()
                        self.add_item_unit_select.reset()
                        if self.running_tare:
                            self.current_state = Main.MainStates.RETARE
                        else:
                            self.current_state = Main.MainStates.SELECT_ITEM

                        
                if self.current_state == Main.MainStates.INIT:
                    if event.key == K_RETURN:
                        self.current_state = Main.MainStates.RETARE
                
                # Save menu.
                if self.current_state == Main.MainStates.SAVE_MEAL:
                    if event.key == K_UP:
                        self.save_to_pantry_or_fitbit_selection.scroll(-1)
                    elif event.key == K_DOWN:
                        self.save_to_pantry_or_fitbit_selection.scroll(1)
                    elif event.unicode.isalpha() or event.unicode == " ":
                        self.save_to_pantry_or_fitbit_input.add_character(event.unicode)
                    if event.key == K_BACKSPACE:
                        self.save_to_pantry_or_fitbit_input.backspace()
                    elif event.key == K_RETURN:
                        text = self.save_to_pantry_or_fitbit_input.get_text()
                        if text:
                            selection = self.save_to_pantry_or_fitbit_selection.get_selected_text()

                            if selection == "Eat now?":
                                fitbit = get_active_fitbit()
                                units = get_gram_unit_id(fitbit)
                                # Create the food.
                                r = fitbit.create_food({
                                    "name" : text,
                                    "defaultFoodMeasurementUnitId" : units,
                                    "defaultServingSize" : self.running_weight,
                                    "calories" : int(self.running_calories),
                                    "formType" : "DRY",
                                    "description" : "Logged by Calorieounter.",
                                    "brand" : "CalorieCounter"
                                })

                                # Get the ID.
                                food_id = r['food']['foodId']
                                
                                fitbit.log_food({
                                    "foodId" : food_id,
                                    "mealTypeId" : get_now_mealtime_id(),
                                    "unitId" : units,
                                    "amount" : self.running_weight,
                                    "date" : datetime.datetime.now().strftime("%Y-%m-%d")
                                })

                            else:
                                # Here is where we add to the pantry that I have yet to create.
                                pass

                            # We are done. Lets go back to main menu.
                            self.escape()
                            return SceneEnum.MODE_SELECTION
                            

                        
                        
                if self.current_state == Main.MainStates.SELECT_ITEM:
                    
                    if event.key == K_BACKSPACE:
                        self.input_box.backspace()
                        self.food_display.set_filter(self.input_box.get_text())
                    elif event.key == K_UP:
                        self.food_display.scroll(-1)
                        name = self.food_display.get_selected_text()
                        if name:
                            self._update_values(name)
                    elif event.key == K_DOWN:
                        self.food_display.scroll(1)
                        name = self.food_display.get_selected_text() 
                        if name:
                            self.live_calories = self.food_map[name[:name.rfind(" (")]]["calories"]
                    elif event.key == K_RETURN and round(self.live_weight) != 0:
                        name = self.food_display.get_selected_text() 
                        if name:
                            name = name[:name.rfind(" (")]
                            self.add_new_item(name)
                            self.input_box.clear()
                            self.food_display.clear_filters()
                    elif event.key == K_LEFT:
                        self.added_items_display.scroll(-1)
                    elif event.key == K_RIGHT:
                        self.added_items_display.scroll(1)
                    elif event.key == K_DELETE:
                        t = self.added_items_display.remove_selected_item()
                        for index, item in enumerate(self.added_items):
                            if str(item) == t:
                                removed_item = self.added_items.pop(index)
                                self.running_calories -= removed_item.calories
                                self.running_weight -= removed_item.weight
                                break
                    elif (get_mods() & KMOD_CTRL) and event.key == K_t and not self.running_tare:
                        self.current_state = Main.MainStates.RETARE

                    elif (get_mods() & KMOD_CTRL) and event.key == K_n and self.input_box.get_text():
                        self.current_state = Main.MainStates.ADD_ITEM
                        self.item_to_add_name = self.input_box.get_text()
                        self.add_item_message.set_text(self.item_to_add_name)

                    # If user elects to save this meal.
                    elif (get_mods() & KMOD_CTRL) and event.key == K_s and self.added_items_display.contains_elements():
                        self.current_state = Main.MainStates.SAVE_MEAL

                    elif event.unicode.isalpha() or event.unicode == " ":
                        self.input_box.add_character(event.unicode)
                        self.food_display.set_filter(self.input_box.get_text())



        return -1

class TestScene(Scene):
    def __init__(self, resolution: List[int]):
        this_font = Font('freesansbold.ttf', 32) 
        self.scene = MonitorBox("Something", (10,10), this_font)
        self.running_text = ""

    def update(self, screen, events, delta):
        screen.fill((0,0,0))
        screen.blit(*self.scene.get_blit())

        for event in events:
            if event.type == KEYDOWN:
                if event.unicode.isalpha() or event.unicode.isnumeric():
                    self.running_text += event.unicode
                    print(self.running_text)
                    print(self.scene.colours)
                if event.key == K_RETURN:
                    self.scene.update_value(self.running_text)
                    self.running_text = ""
